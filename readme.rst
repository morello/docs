**************************************
Morello Platform Software Repositories
**************************************

This is the top level readme for the Morello platform software repositories.
Please refer to the Open Source Software `landing page`_ for more context.

Morello is a research program and code to support the prototype architecture
will not be upstreamed. Support is maintained as a series of forks within
this hosting. The codebase is intended to provide the necessary platform
support to act as a starting point for collaborative ecosystem development.

Open Source Software hosted in the morello namespace on the hosting
===================================================================

Morello platform Open Source Software (OSS) is delivered as an integrated
software stack. Scripting is provided to build and run the complete stack.

Development is continuous - updated code is pushed to mainline branches.

A release is a set of tagged commits, identifying specific components which
have been integration-tested together.

The `Release Notes`_ provide more information on
specific releases.

Branches structure
------------------

``morello/mainline``
  Mainline branch name used for repositories which are not derived from an
  external upstream project. The projects without external upstream are
  started for Morello. These branches intrinsically support Morello.

``morello/[upstream-branch-name]``
  Mainline branch for repositories with an upstream. These branches are based on
  fork points in the corresponding upstream/[upstream-branch-name] branches and host
  the patches adding Morello support. The fork point is specified by the
  morello-[upstream-branch-name]-base tag.

``upstream/[upstream-branch-name]``
  This branch points to the upstream branch named [upstream-branch-name],
  e.g  `master`_  is taken from the master branch of `Trusted Firmware TF-A`_.
  These branches might be updated with upstream changes from time to time
  with the upstream changes being merged to the corresponding Morello branches.

``morello/release-Major.Minor``
  These forks are created from the morello/mainline or
  morello/[upstream-branch-name] branches for the release.


Overview of deliverables
========================

Firmware
--------

Based on standard open source software components: `SCP firmware`_,
`Trusted Firmware TF-A`_, `UEFI EDK2`_

BusyBox
-------

An AArch64 based BusyBox profile demonstrating ACPI and DT boot, and the
platform features.

Linux Purecap Environment
-------------------------

A Debian 11 based environment to demonstrate the Pure-cap applications
executing on a mainline-based fork of Linux kernel with experimental
support for the pure-capability kernel-user ABI (PCuABI).

Integrated Android stack - releases postponed
---------------------------------------------

Android/Morello SW stack with a few Android components/workloads
ported to Morello.

Please refer to the `Android/Morello readme`_ for details on earlier releases.

Toolchain
---------

Morello is supported by LLVM-based open-source toolchains. These are
experimental toolchains and some features may be missing.

Please refer to the `Morello LLVM Toolchain readme`_ for more details.

CheriBSD
========

CheriBSD is a FreeBSD-based implementation of a memory-safe kernel and
userspace supporting CHERI-MIPS, CHERI-RISC-V and Morello, developed by
the University of Cambridge Computer Laboratory and SRI International as
part of the CHERI project. It includes example ports of application
frameworks and demonstrates a more complete integration of CHERI (and
Morello) support into an OS design. CheriBSD is maintained and hosted by
the University of Cambridge. More information is available at their
`CheriBSD Morello page`_ and `CheriBSD repository mirror`_.

Development platforms
=====================

Morello Platform Model
----------------------

The Morello Platform Model is an open access FVP (Fixed Virtual Platform)
implementation of the development platform which is available to download
from Arm's `Ecosystem FVP Developer page`_

Morello Development Board
-------------------------

The Morello development board is a hardware platform that integrates the
Morello System on Chip (SoC). More information is
available at `Morello SoC TRM page`_.

Getting started
===============

Please follow the relevant ``User Guide`` below to sync, build, and run the stack.

* `Firmware Boot`_

Build and boot the firmware components i.e. SCP firmware, TF-A and UEFI
bootloader. This guide also provides information about running the following
firmware workloads:

  * Busybox based initramfs
  * Standard Ubuntu 20.04 distribution

* `Linux Purecap Environment Boot`_

A Debian 11 based environment to demonstrate the Pure-cap applications
executing on a mainline-based fork of Linux kernel (6.1) with experimental
support for the pure-capability kernel-user ABI (PCuABI).

Support and Contributions
=========================

- The Morello Gitlab and associated issues' trackers are intended to enable
  Open Source Software development - supporting engineering contributions and
  targeted defects and patches relating to specific component projects. We
  welcome engineering collaboration.
- Wider support queries and questions should be raised via Arm’s `Morello forum`_.
- For questions specific to the CheriBSD environment visit https://www.cheribsd.org/

Report security vulnerabilities
===============================

For reporting security vulnerabilities, please refer to the
`Vulnerability reporting`_ page.


.. _landing page:
 https://www.morello-project.org

.. _Firmware Boot:
 firmware/user-guide.rst

.. _Release Notes:
 release-notes.rst

.. _Android/Morello User Guide:
 android/user-guide.rst

.. _Linux Purecap Environment Boot:
 linux/user-guide.rst

.. _Android/Morello readme:
 android/readme.rst

.. _Morello LLVM Toolchain readme:
 toolchain-readme.rst

.. _Vulnerability reporting:
 ../common/vulnerability-reporting.rst

.. _Morello forum:
 https://community.arm.com/developer/research/morello/f/forum

.. _Ecosystem FVP Developer page:
 https://developer.arm.com/tools-and-software/open-source-software/arm-platforms-software/arm-ecosystem-fvps

.. _SCP firmware:
 https://github.com/ARM-software/SCP-firmware

.. _Trusted Firmware TF-A:
 https://www.trustedfirmware.org/

.. _master:
 https://git.morello-project.org/morello/trusted-firmware-a/-/tree/upstream/master

.. _UEFI EDK2:
 https://github.com/tianocore/edk2

.. _CheriBSD Morello page:
 https://morello-dist.cl.cam.ac.uk

.. _CheriBSD repository mirror:
 https://git.morello-project.org/university-of-cambridge/mirrors/cheribsd

.. _Morello SoC TRM page:
 https://developer.arm.com/documentation/102278/0000

