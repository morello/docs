###################
Firmware User Guide
###################

.. section-numbering::
    :suffix: .

.. contents::

This document provides the steps required to source, build and run the firmware
components i.e. SCP firmware, TF-A and UEFI bootloader as well as steps to boot
the following supported firmware workloads:

* Busybox Boot: Build and boot an AArch64 based BusyBox initramfs to demonstrate
  ACPI and DT boot along with the platform features

* Ubuntu Boot: Boot a standard Ubuntu 20.04 distribution

Workloads are used to test the firmware components under a standard OS
environment. See the workload-specific part of each section for your usecase.

*************
Prerequisites
*************

Set up the host environment according to the `prerequisites`_ section before
continuing.

************
Docker setup
************

Syncing and building the Morello software stack can also be implemented using
the docker environment. The details on the docker environment and the steps to
set it up are mentioned in the `Docker README`_. Once the docker environment
is set and a docker container is up and running, use the following command to
enter into the conatiner:

::

        docker exec -it -u morello morello-firmware /bin/bash

This docker container can be used for syncing the source code and building the
firmware binaries out of it. All the commands mentioned in this guide for
syncing and building the firmware can executed from the docker conatiner.

**Note:** A share mount-point volume named ``workspace`` is created while setting
up the docker and the same can be accessed from the host to interact with the
stack. Once built, the firmware binaries can be obtained from ``workspace/output``.

***********************
Syncing the Source Code
***********************

**Note:** If docker is not used, first create a new folder that will be your
workspace.

::

        mkdir workspace
        cd workspace

Run the following commands to fetch the software stack sources.

**Note:** To have a shallow fetch of the stack use ``--depth=1`` with the
following *repo init* command. This fastens the repo sync process but results
in providing the limited commit history for the projects fetched.

::

    repo init \
        -u https://git.morello-project.org/morello/manifest.git \
        -b <BRANCH> \
        -g <GROUP>
    repo sync

The ``-g <GROUP>`` option is to avoid downloading all components for all
supported software stacks and save time and storage space.

.. list-table::
   :widths: 25 25
   :header-rows: 1

   * - usecase/workload
     - ``-g <GROUP>``
   * - Firmware only
     - bsp
   * - Busybox
     - busybox
   * - Ubuntu
     - bsp


Supported options for ``-b <BRANCH>`` are:

* morello/integration-<major>.<minor>
  (This option fetches the release branch of a given major and minor version
  and as of today it is valid only for Release-1.6)
* morello/release-<major>.<minor>
  (This option fetches the release branch of a given major and minor version
  for releases delivered till Release-1.5)
* refs/tags/<TAG>
  (This option fetches a particular release tag pointed by the <TAG> field
  for the releases delivered till Release-1.6)
* morello/mainline
  (This option fetches the mainline branch used for code development and could
  contain patches that are not available in the above branches/tags.)

**Note:** ``repo sync`` may take significant amount of time to sync the
software stack based on the host machine and network connectivity.

************************
Building the Source Code
************************

All software components are built using a set of bash based build scripts which
are checked out under ``workspace/build-scripts`` directory. These
scripts are common for both FVP and Development Board and the build is
differentiated using build arguments which are explained in the coming sections.

There are separate scripts available to build each software component and
scripts to package the binaries that can be used directly on the
target platform.

Check dependencies
##################

Before starting the software build, ensure that all the required packages are
installed by running the following command.

::

  ./build-scripts/check_dep.sh

**Note:** Add the argument ``fvp`` to the above mentioned command for checking
the FVP specific dependencies as well.

A message of "no missing dependencies detected" indicates that all the required
packages are installed on the host machine.

Building the Software Stack
###########################

To build all the software components for a given platform and filesystem, a top
level `build-all.sh` script is provided.
Execute the following command to trigger the software build.

::

  ./build-scripts/build-all.sh -p <PLATFORM_TYPE> -f <WORKLOAD> <CMD>

Supported options for ``<PLATFORM_TYPE>`` are:

* fvp
* soc

If -p flag is not passed, the build scripts default to ``fvp``
for ``PLATFORM_TYPE``.

Supported options for ``WORKLOAD`` are:

.. list-table::
   :widths: 25 25
   :header-rows: 1

   * - Workload
     - ``-f <WORKLOAD>``
   * - Firmware only
     - none
   * - Busybox
     - busybox
   * - Ubuntu
     - none

Supported options for ``<CMD>`` are:

* clean
* build
* all

If ``CMD`` option is not passed, ``build`` is performed by default.

**Note:** If -p flag is changed, ``clean`` should be performed before a
``build``.

Once the build is complete, the build artifacts are available under
``workspace/output`` directory.


Following section provides the build commands for building different workload
options supported.

Firmware only
*************

Use this option to build only the firmware components.

**Note:** If user wants to build AArch64 firmware binaries (firmware without
Morello capability features), the build configuration flag
``stack_morello_capabilities`` (which is defined in
``workspace/build-scripts/config/bsp`` file) should be set to 0 and
the entire stack should be rebuild. AArch64 firmware binaries are only
compatible with OS built without Morello capability features.

::

  ./build-scripts/build-all.sh -p <fvp/soc> -f none


Ubuntu Distribution
*******************

Morello platform provides support for installation and boot of standard Ubuntu
20.04 Distribution. The distribution is installed on a SATA disk and since the
installed image is persistent it can be used for multiple boots.

**Note:** This distribution does not contain any capability-aware software. It
is a standard AArch64 image to support an environment for future development.

Build the firmware files:

::

  ./build-scripts/build-all.sh -p <fvp/soc> -f none

Minimal BusyBox
***************

An AArch64 based BusyBox initramfs to demonstrate ACPI and DT boot, and the
platform features

**Note:** Only the Linux kernel is capability-aware whereas BusyBox binary
is AArch64.

Build BusyBox:

::

  ./build-scripts/build-all.sh -p <fvp/soc> -f busybox


Building individual software components
***************************************

The build system supports building individual software components if user has
only modified a given software component to avoid rebuilding the unmodified
components. However the packaging step has to be done manually to generate
the final image containing the updated software component.

The following table shows the command to use to build a given software component
and the corresponding package command to execute to update the build artifacts.

**Note:** For a given filesystem, the whole software stack should be built first
with the build-all.sh script as explained above before building the individual
software components with the commands provided in the table.

.. list-table::
   :widths: 20 150 150
   :header-rows: 1

   * - Software Component
     - Build Command
     - Package Command
   * - SCP
     - ./build-scripts/build-scp.sh -p <fvp/soc> -f none
     - ./build-scripts/build-firmware-image.sh -p <fvp/soc> -f none
   * - ARM-TF
     - ./build-scripts/build-arm-tf.sh -p <fvp/soc> -f none
     - ./build-scripts/build-firmware-image.sh -p <fvp/soc> -f none
   * - UEFI
     - ./build-scripts/build-uefi.sh -p <fvp/soc> -f none
     - ./build-scripts/build-firmware-image.sh -p <fvp/soc> -f none
   * - Linux
     - ./build-scripts/build-linux.sh -p <fvp/soc> -f <filesystem>
     - ./build-scripts/build-disk-image.sh -p <fvp/soc> -f <filesystem>
   * - Busybox
     - ./build-scripts/build-busybox.sh -p <fvp/soc> -f busybox
     - ./build-scripts/build-disk-image.sh -p <fvp/soc> -f busybox


********************
Running the Software
********************

Running the Software on FVP
###########################

The Morello Fixed Virtual Platform (Morello FVP) must be available
to test the software.

Please refer to `Arm Ecosystem FVPs`_ to download the Morello FVP.

Busybox Boot
************

``workspace/run-scripts/`` provides scripts
for running the software stack on the Morello FVP platform.


The run-scripts structure is as follows:

::

  run-scripts
    |--run_model.sh
    |-- ...

Ensure that all the dependencies are met by executing the
FVP: ``./path/to/FVP_Morello/models/Linux64_GCC-6.4/FVP_Morello``.
You should see the FVP launch, presenting a graphical interface
showing information about the current state of the FVP.

The ``run_model.sh`` script in ``workspace/run-scripts``
will launch the FVP, providing the previously built images as arguments.
Execute the ``run_model.sh`` script:

::

  ./run-scripts/run_model.sh -m <model binary path> -f busybox

When the script is executed, five uart terminal instances will be launched, one
for the SCP, one for MCP and three for the AP (Application Processor). Once the
FVP is running, the SCP will be the first to boot, bringing the AP out of reset.
The AP will start booting Trusted Firmware-A, then UEFI, then BusyBox/Ubuntu.

To run terminal-only mode on hosts without graphics/display:

::

  env -u DISPLAY ./run-scripts/run_model.sh -m <model binary path> -f busybox

This launches FVP in the background and automatically connects to the
interactive application console via telnet.

To stop the model, exit telnet:

::

  Ctrl + ]
  telnet> close

For other FVP related usecases please see `Morello FVP Features`_


Ubuntu Distribution
*******************

Installation
^^^^^^^^^^^^

Create an empty SATA disk image for installation and boot of the distribution.

::

  dd if=/dev/zero of=ubuntu.satadisk bs=1G count=16

Also download the LTS Ubuntu ISO image of the required Ubuntu version from `Ubuntu ISO`_


Use the ``run_model.sh`` script as given in the previous ``Busybox Boot``
section with additional options to boot Ubuntu on the FVP model:
::

  ./run-scripts/run_model.sh -m <model binary path> -f distro -d <sata disk path> -i <ISO image path>

Boot
^^^^

Run the FVP model with the following parameters to boot the distribution which
is installed on the SATA disk:

::

  ./run-scripts/run_model.sh -m <model binary path> -f distro -d <sata disk path>


Running the Software on Development Board
#########################################

See the `Setting up the Morello Board`_ section before continuing

Update firmware on microSD card
*******************************

Prebuilt board and SoC firmware
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The board and SoC firmware prebuilt binaries are made available under
``workspace/bsp/board-firmware/``. These binaries can be copied to
the onboard microSD card to boot the platform to the UEFI menu.

**Note:** Prebuilt binaries of AArch64 firmware (without Morello capability
features) are available under the following directory:
``workspace/bsp/board-firmware/SOFTWARE/AARCH64/``.

SoC firmware built from source
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

SoC firmware binaries built from source are available under
``workspace/output/soc/firmware/``. The binaries from this folder
can be copied to the SOFTWARE folder in microSD card for more recent build
of the firmware or if the firmware source has been modified and rebuilt.

Steps to update microSD card with the firmware binaries
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* The USB debug cable when connected to the host machine will show
  the microSD partition on the host machine which can be mounted.

  ::

    $> sudo mount /dev/sdX1 /mnt
    $> sudo rm -rf /mnt/*
    $> sudo cp -r workspace/bsp/board-firmware/* /mnt/
    $> sudo umount /mnt

**NOTE**: replace ``sdX1`` with the device and partition of the SD card.

* The SD card folder structure should now look like the following:

  ::

    ├── LIB
    ├── LICENSES
    ├── MB
    ├── SOFTWARE
    ├── config.txt
    └── ee0364b.txt

  It consists of the configuration file (config.txt), the Motherboard controller
  firmware (MB/) and the last working software binaries (SOFTWARE/) that boot
  till the UEFI menu.
* Copying the board firmware (MB + SOFTWARE) is required only once to get the
  versions of all the images from the last release
* If the user is intended to work with the ``mainline`` branch, always replace
  the software binaries at ``/mnt/SOFTWARE`` with freshly built ones from
  ``workspace/output/soc/firmware/``
* If the user wants to run the prebuilt AArch64 firmware (without Morello
  capability features) then the binaries under
  ``workspace/bsp/board-firmware/SOFTWARE/AARCH64/`` can be copied
  to the ``/mnt/SOFTWARE/`` directory. Once copied the board shall be rebooted
  for the new firmware to be booted. AArch64 firmware binaries are only
  compatible with OS built without Morello capability features.
* After copying the binaries run the REBOOT command from the
  MCC console. This will update the board firmware images (if newer),
  power on the supply rails, configure IOFPGA, program SCC registers from
  ``/mnt/MB/HBI0364B/io_v010f.txt`` file and boot the SCP/MCP/AP cores.

  ::

    Cmd> REBOOT

**Note on changing SoC HW/FW configuration:**

- SCP, ARM-TF and UEFI firmware have been designed to be flexible enough in
  configuring the system by parsing the SCC BOOT_GPRx registers. These
  registers are configured by the user in ``/mnt/MB/HBI0364B/io_v010f.txt``,
  which will then be programmed by the MCC into the SoC SCC registers over
  serial interface during the MCC reboot process.
- More details on the utilization of SCC BOOT_GPR registers can be found in
  ``/mnt/MB/HBI0364B/io_v010f.txt``

Boot BusyBox Image
******************

Prepare a bootable disk
^^^^^^^^^^^^^^^^^^^^^^^

* A bootable disk can be prepared by flashing the image generated from the
  source build onto a USB drive.
* Connect bootable disk (USB drive) into a host machine that
  is running Linux.
* Use the following commands to prepare the GRUB image on the
  USB drive

  ::

    $ lsblk
    $ sudo dd if=workspace/output/soc/busybox.img of=</dev/sdX> conv=fsync bs=1M      # write the disk image to
                                                                                      # /dev/sdX
    $ sudo sgdisk -e </dev/sdX>                                                       # adjust the primary and
                                                                                      # alternate header on /dev/sdX
    $ sync

**Note:** Replace ``</dev/sdX>`` with the handle corresponding to your USB drive,
as identified by the ``lsblk`` command.

**Note:** All supported images are partitioned using GPT.
For details and issues related to the location of the alternate header, refer
to the `troubleshooting guide`_.

Booting the board with BusyBox Image
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Insert the bootable disk created earlier to the Morello board. Reboot the
board by issuing the following command in the MCC console:

::

  Cmd> REBOOT


Switch to the AP UART console. Once the application core has started
booting the UEFI firmware, enter the UEFI menu by pressing the Esc key within
the 10s UEFI boot timeout. Enter the UEFI Boot Manager
menu and select the disk.

By default the Linux kernel will boot with ACPI configuration:

::

  | BusyBox Morello Platform (Device Tree)                                     |
  |*BusyBox Morello Platform (ACPI)                                            |

The system will boot into a BusyBox Linux image environment.

Network Boot
------------
The Morello UEFI (both SoC and FVP builds) is by default built with support
for PXE (which uses TFTP) and HTTP (but not HTTPS) network booting.
Check the documentation for your distribution of choice on how to configure it
for the network boot.
When doing HTTP network boot, the UEFI supports loading a `ramdisk`_
downloaded over the network.
The generated disk image, ``busybox.img``, can be used directly without any modification.

Boot Ubuntu Image
*****************

Install Ubuntu
^^^^^^^^^^^^^^

Two mass storage devices are required: a USB drive to flash the installer
(downloaded LTS Ubuntu ISO image) and a SATA drive to install the Ubuntu.
The Morello board already has an empty SATA drive connected and secured in an
ATX case.

Connect USB drive to the host machine. Use the following commands to
prepare the image on a USB drive:

::

  $ lsblk
  $ sudo dd if=<Ubuntu image name> of=/dev/sdX conv=fsync bs=1M
  $ sync

Note: Replace ``/dev/sdX`` with the handle corresponding to your USB drive
as identified by the ``lsblk`` command.

Insert the bootable installer disk created earlier (USB drive) to one of the
four USB3 ports in the backside of the Morello board.

Reboot the board by issuing the following command
in the MCC console:

::

  Cmd> REBOOT

Switch to the AP UART console. Once the application core has started
booting the UEFI firmware, enter the UEFI menu by pressing the Esc key within
the 10s UEFI boot timeout. Enter the UEFI Boot Manager
menu and select the disk (USB drive) with the installer.
Select the appropriate option from the installer.
Once the installation is complete, remove the disk (USB drive) which
contains the installer.

Booting the Board with an Ubuntu Image
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Reboot the board by issuing the following command in the MCC console:

::

  Cmd> REBOOT

Switch to the AP UART console. Once the application core has started
booting the UEFI firmware, enter the UEFI menu by pressing the Esc key within
the 10s UEFI boot timeout. Enter the UEFI Boot Manager menu and select the
Ubuntu-installed disk (SATA drive).

The system will boot into an Ubuntu environment.


.. _prerequisites:
 ../common/prerequisites.rst

.. _ramdisk:
 https://github.com/tianocore/tianocore.github.io/wiki/HTTP-Boot#ram-disk-boot-from-http

.. _troubleshooting guide:
 ../common/troubleshooting-guide.rst

.. _Arm Ecosystem FVPs:
 https://developer.arm.com/tools-and-software/open-source-software/arm-platforms-software/arm-ecosystem-fvps

.. _Setting up the Morello Board:
 ../common/board-setup.rst

.. _Ubuntu ISO:
 https://ubuntu.com/download/server/arm

.. _Morello FVP Features:
 ../common/fvp-features.rst

.. _Docker README:
 https://git.morello-project.org/morello/morello-firmware-docker/-/blob/latest/README.md
