#########
Changelog
#########

.. section-numbering::
    :suffix: .

.. contents::

This document contains a summary of the incremental features, changes, fixes and
known issues in the firmware for each release of the Morello stack. It is listed
in inverse chronological order.

Morello Release-1.8
===================

New Platform Features and Fixes
-------------------------------

* Add support for Docker environment to build firmware.
* Route HDMI PHY interrupt from IOFPGA to SoC.

Platform Support
----------------

* This Software release is tested on ``r0p1`` and ``r0p2`` versions of the
  Morello SoC.
* This Software release is tested on Fast Model Platform (FVP) version - Fast
  Models [0.11.34 (Nov 14 2022)].

Morello Development Board specific Known Issues and Limitations
---------------------------------------------------------------

* It is recommended to run DMC Bing exclusively in Server mode. However, it may
  be possible to use Client mode for certain kinds of performance testing. For
  this type of use, we recommend that you work closely with Arm.
* PCIe root port is limited to GEN3 speed due to the on-board PCIe switch itself
  only supporting up to GEN3 speed.
* CCIX traffic is not supported over CCIX RC.
* Multiple UEFI shell entries in UEFI Boot Manager Menu after enabling UEFI
  persistent variables storage.
* PCC reset issued via the PCC CLI interface doesn't work and ends up corrupting
  the PCC firmware state.
* EFI GOP is unconditionally active with a fix resolution of 1920x1080 at 60Hz.
* Occasional I2C transmission error logs on the SCP console during the DVFS
  operation.
* Only the firmware support for Coresight has been added. To get the Coresight
  functional on the Morello board, the kernel needs to be built with `this patch`_.

Morello Release-1.7
===================

New Platform Features and Fixes
-------------------------------

* Add CPU idle support.
* Add DT entries for thermal zones.
* Add DT entries for I2S-audio support.
* Enable hardware driven periodic sampling of temperature and voltage sensor values.
  Move to interrupt based temperature sensor alarm and shutdown handling.
* Add firmware version information, for Morello SoC, to Type 45 SMBIOS tables for
  the following components: MCC, PCC, SCP, TF-A, EDK2 and EDK2-Platforms.

  * This can be viewed in UEFI Shell using the command `smbiosview -t 45`. Each
    table provides a component's firmware version and commit ID.

Platform Support
----------------

* This Software release is tested on ``r0p1`` and ``r0p2`` versions of the
  Morello SoC.
* This Software release is tested on Fast Model Platform (FVP) version - Fast
  Models [0.11.34 (Nov 14 2022)].

Morello Development Board specific Known Issues and Limitations
---------------------------------------------------------------

* It is recommended to run DMC Bing exclusively in Server mode. However, it may
  be possible to use Client mode for certain kinds of performance testing. For
  this type of use, we recommend that you work closely with Arm.
* PCIe root port is limited to GEN3 speed due to the on-board PCIe switch itself
  only supporting up to GEN3 speed.
* CCIX traffic is not supported over CCIX RC.
* Multiple UEFI shell entries in UEFI Boot Manager Menu after enabling UEFI
  persistent variables storage.
* PCC reset issued via the PCC CLI interface doesn't work and ends up corrupting
  the PCC firmware state.
* EFI GOP is unconditionally active with a fix resolution of 1920x1080 at 60Hz.
* Occasional I2C transmission error logs on the SCP console during the DVFS
  operation.
* Only the firmware support for Coresight has been added. To get the Coresight
  functional on the Morello board, the kernel needs to be built with `this patch`_
  which will be available in the next Morello Linux rebase.

Morello Release-1.6.1
=====================

**NOTE:** This release is intended to support the `Morello Linux Environment`_ and
`CheriBSD`_ only.

New Platform Features and Fixes
-------------------------------

* Add PMIC driver in SCP to enable DVFS with DT boot.
* Add Coresight support in firmware.
* Fix the GPU interrupts ordering in device tree.
* Fix the USB HID input device/hub hang issue in EDK2.
* Fix the SPI interrupts mapping in TF-A.
* Update the MCC firmware binary to v233 adding support to fetch silicon
  revision from the EEPROM.
* Rebase the firmware components (SCP, TF-A, EDK2, EDK2-Platforms) from their
  respective upstream projects.
* Update the firmware binaries to the following versions:

  * SCP-Firmware      : d163a087651dd47d5002033e2afb2f6d96054ea0
  * Trusted Firmware-A: 6616cbf1323d883c8c1e67e9944fc05d8f319284
  * EDK2              : aa9389302041654a3118c55c9b3cc77a1f35f543
  * EDK2-Platforms    : 958487af8ffdd0c5d8d201cb02bc0c40d3e44af3

Platform Support
----------------

* This Software release is tested on ``r0p1`` and ``r0p2`` versions of the
  Morello SoC.
* This Software release is tested on Fast Model Platform (FVP) version - Fast
  Models [0.11.34 (Nov 14 2022)].

Morello Development Board specific Known Issues and Limitations
---------------------------------------------------------------

* It is recommended to run DMC Bing exclusively in Server mode. However, it may
  be possible to use Client mode for certain kinds of performance testing. For
  this type of use, we recommend that you work closely with Arm.
* PCIe root port is limited to GEN3 speed due to the on-board PCIe switch itself
  only supporting up to GEN3 speed.
* CCIX traffic is not supported over CCIX RC.
* Multiple UEFI shell entries in UEFI Boot Manager Menu after enabling UEFI
  persistent variables storage.
* PCC reset issued via the PCC CLI interface doesn't work and ends up corrupting
  the PCC firmware state.
* EFI GOP is unconditionally active with a fix resolution of 1920x1080 at 60Hz.
* Occasional I2C transmission error logs on the SCP console during the DVFS
  operation.
* Only the firmware support for Coresight has been added. To get the Coresight
  functional on the Morello board, the kernel needs to be built with `this patch`_
  which will be available in the next Morello Linux rebase.

Morello Release-1.6
===================

New Platform Features
---------------------

* Morello Program logo support added in UEFI firmware
* Device Tree support added in UEFI firmware
* Fetch the Silicon Revision from MCC and update it in UEFI firmware
* Add GPU Device Tree entry, tested against custom linux based AArch64 debian
  distribution.

Platform Support
----------------

* This Software release is tested on r0p1 version of the Morello SoC.
* This Software release is tested on Fast Model Platform (FVP) version - Fast
  Models [0.11.34 (Nov 14 2022)].

Morello Development Board specific Known Issues and Limitations
---------------------------------------------------------------

* It is recommended to run DMC Bing exclusively in Server mode. However, it may
  be possible to use Client mode for certain kinds of performance testing. For
  this type of use, we recommend that you work closely with Arm.
* PCIe root port is limited to GEN3 speed due to the on-board PCIe switch itself
  only supporting up to GEN3 speed.
* CCIX traffic is not supported over CCIX RC.
* The boot might hang if any USB HID input device (USB mouse / USB keyboard) is
  connected. To workaround the hang, user can trigger input from the connected
  device (any key press from a USB keyboard and/or mouse clicks from a USB
  mouse).
* The boot might hang if USB hubs are connected. It is advisable that your boot
  flow does not depend on using a USB hub. For example, when booting from a USB
  MSD connect it directly to the board. The current workaround is physically to
  disconnect and re-connect the hub and the boot should continue (no need for
  reboot).
* Multiple UEFI shell entries in UEFI Boot Manager Menu after enabling UEFI
  persistent variables storage.
* PCC reset issued via the PCC CLI interface doesn't work and ends up corrupting
  the PCC firmware state.
* EFI GOP is unconditionally active with a fix resolution of 1920x1080 at 60Hz.
* GPU interrupts in Device Tree are defined in an incorrect order which impacts
  only CheriBSD and causes boot failure with CheriBSD 22.12 installer. Refer
  `GPU DT Issue`_ for more details.


See `Release Notes`_ for information on older revisions

.. _Release Notes: ../deprecated/release-notes.rst

.. _GPU DT Issue:
 https://git.morello-project.org/morello/trusted-firmware-a/-/issues/1

.. _this patch:
 https://git.kernel.org/pub/scm/linux/kernel/git/coresight/linux.git/commit/?id=ab5ca6268afc

.. _Morello Linux Environment:
 https://linux.morello-project.org/

.. _CheriBSD:
 https://www.cheribsd.org/
