*************
Release notes
*************

.. section-numbering::
    :suffix: .

.. contents::

This document contains a summary of the incremental features, changes,
fixes and known issues in each release of the Morello stack.
It is listed in inverse chronological order.

Tagged Version - **morello-release-1.5**
========================================

New Features
------------

The following are a summary of the key software features of this release:

* Migrated build environment to Ubuntu Linux 20.04 LTS
* Android/Morello

  * Pure-cap kernel/user ABI support

    * Add AT_CHERI_STACK_CAP root capability to auxv
    * Change AT_PHDR entry to bounded capability
    * Consider AT_BASE an address instead of a capability
    * Limit bounds/permissions of PCC
    * Make sure DSO bounds don't intersect
    * Refine dl_phdr_info
    * Remove rootcap concept
    * Restrict AT_CHERI_{EXEC,INTERP}_{RW,RX}_CAP auxv entries
    * Use argc/argv/envp/auxv from C0-C3 in Pure-cap
  * Switch to new Pure-cap variadic PCS
  * Refine dynamic linkage support

    * Set bounds in TLSDESC resolver
    * Support TPREL
    * Refine handling of omitted TLS symbols
    * Refine imported symbol permissions
  * Refine std::equal_to, std::hash, std::sort
  * Remove deprecated libarchcap interface
  * Deprecate MORELLO-META markup
  * Port pcre2 to Pure-cap and enable the tests
  * Port second-stage init to Pure-cap
  * Switch on sys.use_memfd for compatibility with kernels without ashmem support
  * Import optimized Morello memcpy/memset
  * Generate Morello assembly system call sequences
  * Fix dlfcn.segment_gap

* Linux kernel

  * Switched to mainline-based branch (5.18 as of this release)
  * Experimental support for the pure-capability kernel-user ABI (PCuABI);
    must be explicitly selected. See
    `this section of the Morello kernel documentation`_ for more information.
  * The Morello extension is no longer runtime-detected. This means that a
    kernel configured with ``CONFIG_ARM64_MORELLO=y`` will now only boot on
    a Morello-enabled platform.

* Toolchain

  * ABI updates

    * Uses the updated varargs PCS by default
    * The purecap thread local storage now supports all TLS models
  * Newlib/libgloss now supports processing RELATIVE relocations for initializing
    capabilities, in addition to the already supported caprelocs style relocations.
  * Code generation improvements for purecap

    * Fixed issue where accessing globals could cause artificially high register
      pressure.
    * Global merging is now enabled by default, further reducing register pressure
      when accessing globals and potentially reducing the number of loads required
      to generate addresses of globals.
    * Can now generate ccmp instructions when comparing capabilities
  * Mixing objects with different ABIs is now an error.

* Linux Purecap Environment

  * A Debian 11 based environment to demonstrate the Pure-cap applications
    executing on a mainline-based fork of Linux kernel (5.18) with experimental
    support for the pure-capability kernel-user ABI (PCuABI).
  * Morello support-enabled BusyBox file-system with sample pure-cap applications
    packaged to the Debian root file-system.
  * Musl, used to compile the purecap applications, is built with `libshim`
    disabled.
  * Prebuilt LLVM toolchain, and Musl binaries packaged with the environment.
  * Support to use Morello Board as a development platform for the Linux
    Purecap Environment.

* Platform Features

  * EFI GOP support
  * SCP->PCC communication path changed to I2C
  * SBBR Compliant Aarch64 Binaries included
  * Unified ROM binaries for SoC & FVP and hosted at `ROM Binaries`_
  * CheriBSD shutdown/reboot EL3 exception issue fixed in TF-A
  * MCC Refresh to introduce new commands to fetch firmware version
  * DDR Dual Rank (64Gb) support
  * Fixed UEFI variable flash storage corruption
  * AP cores now boot at 2.5GHz

Platform Support
----------------

* This Software release is tested on r0p1 version of the Morello SoC.
* This Software release is tested on Fast Model Platform (FVP)
  version - Fast Models [0.11.34 (Nov 14 2022)].

Known issues or Limitations
---------------------------

* The Morello toolchain, which is used to build Android, is based on a much
  more recent version of LLVM than the default prebuilt toolchain in Android 11.
  As a result, a large number of warnings are emitted during the build.
* While booting Android Nano, there are warnings or error messages which are
  expected and normal given the reduced profile of the Android - ``avc`` audit
  reports, ``libprocessgroup`` missing profile messages, ``apex`` warnings,
  ``setpgid`` warnings etc.
* On Android, C++ exceptions in the Pure-cap ABI are not supported.
* The `BinderLibTest.SchedPolicySet` libbinder unit test is disabled because
  it is only supported on the Android kernels.

Morello Development Board specific Known Issues and Limitations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* It is recommended to run DMC Bing exclusively in Server mode. However, it
  may be possible to use Client mode for certain kinds of performance testing.
  For this type of use, we recommend that you work closely with Arm.
* PCIe root port is limited to GEN3 speed due to the on-board PCIe switch
  itself only supporting up to GEN3 speed.
* CCIX traffic is not supported over CCIX RC.
* The boot might hang if any USB HID input device (USB mouse / USB keyboard)
  is connected. To workaround the hang, user can trigger input from the
  connected device (any key press from a USB keyboard and/or mouse clicks from
  a USB mouse).
* The boot might hang if USB hubs are connected. It is advisable that your boot
  flow does not depend on using a USB hub. For example, when booting from a USB
  MSD connect it directly to the board. The current workaround is physically to
  disconnect and re-connect the hub and the boot should continue (no need for
  reboot).
* Multiple UEFI shell entries in UEFI Boot Manager Menu after enabling
  UEFI persistent variables storage.
* PCC reset issued via the PCC CLI interface doesn't work and ends up
  corrupting the PCC firmware state.
* EFI GOP is unconditionally active with a fix resolution of 1920x1080 at 60Hz.

Tagged Version - **morello-release-1.4**
========================================

New Features
------------

The following are a summary of the key software features of this release:

* UEFI persistent variables storage.
* Network boot (HTTP & PXE) support in UEFI.
* Morello sensor library included as part of the board firmware to provide
  support for SoC temperature sensors.
* System Control Processor (SCP) firmware and ARM Trusted firmware rebased
  to their respective upstream repositories.

Platform Support
----------------

* This Software release is tested on r0p0/r0p1 version of the Morello SoC.
* This Software release is tested on Fast Model Platform (FVP)
  version - Fast Models [0.11.33 (Feb 17 2022)].

Known issues or Limitations
---------------------------

* The Morello toolchain, which is used to build Android, is based on a much
  more recent version of LLVM than the default prebuilt toolchain in Android 11.
  As a result, a large number of warnings are emitted during the build.
* While booting Android, there are warnings or error messages which are
  expected and normal given the reduced profile of the Android - ``avc`` audit
  reports, ``libprocessgroup`` missing profile messages, ``apex`` warnings,
  ``setpgid`` warnings etc.
* On Android, C++ exceptions in the Pure-cap ABI are not supported.
* Bionic unit test ``dlfcn.segment_gap`` fails when built for the Hybrid-cap
  ABI, which is caused by executable and non-executable sections being
  allocated onto the same virtual page according to the ELF header of
  the libsegment_gap_outer.so DSO.

Morello Development Board specific Known Issues and Limitations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- It is recommended to run DMC Bing exclusively in Server mode. However, it
  may be possible to use Client mode for certain kinds of performance testing.
  For this type of use, we recommend that you work closely with Arm.
- PCIe root port is limited to GEN3 speed due to the on-board PCIe switch
  itself only supporting up to GEN3 speed.
- CCIX traffic is not supported over CCIX RC.
- The boot might hang if any USB HID input device (USB mouse / USB keyboard)
  is connected. To workaround the hang, user can trigger input from the
  connected device (any key press from a USB keyboard and/or mouse clicks from
  a USB mouse).
- Multiple UEFI shell entries in UEFI Boot Manager Menu after enabling
  UEFI persistent variables storage.
- Occasional hang during UEFI boot with the following assert from VariableRuntimeDxe:
  ``ASSERT [VariableRuntimeDxe] Variable.c(2490): VarNameSize != 0``.
  (Please refer to the `Troubleshooting Guide`_ in case this issue is observed)
- PCC reset issued via the PCC CLI interface doesn't work and ends up
  corrupting the PCC firmware state.

Tagged Version - **morello-release-1.3**
========================================

New Features
------------

The following are a summary of the key software features of this release:

* Android/Morello additional supported features:

  - refine relocations handling and IRELATIVE processing in the Pure-cap ABI
  - support _r_debug in the Pure-cap ABI
  - implement coalescing of previously split memory regions in jemalloc in the Pure-cap ABI
  - libshim: set bounds and permissions for argv and envp elements in the Pure-cap ABI
  - split getauxval into getauxval/getauxptr
  - add experimental AT_CHERI auxv members
  - enable full Android profile (with graphics in software rendering mode)
  - network mode switched to DHCP instead of static IP.

  For more details about Android/Morello, please refer to `Android readme`_.
* The AArch64 Busybox environment provides a set of Morello purecap sample
  applications. These purecap applications are available in the default
  software package for Morello FVP/Development Board and act as a
  demonstrator for the capability features. For more details,
  please refer to https://git.morello-project.org/morello/morello-aarch64.
* System Control Processor (SCP) firmware rebased to latest upstream commit
  with the addition of Morello SoC support.
* ARM Trusted firmware rebased to latest upstream commit with the addition of
  Morello SoC support. The trusted firmware boot flow is now changed from
  RESET_TO_BL31 flow to standard TBBR boot flow.
* EDK2 & EDK2-PLATFORMS rebased to latest upstream commit with
  the addition of Morello SoC support.
* Network mode switched to DHCP instead of static IP.

Morello Development Board specific Features
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Morello SoC with capability aware Rainier CPUs running at
  2.5GHz frequency maximum.
* Dynamic frequency scaling operation of CPUs in device tree mode.
* Dual channel DDR4 RDIMM modules running at 1466MHz (2933MT/s).
* RDIMM modules supported by the firmware:
  - MTA9ASF1G72PZ-2G9E1 (8GB)
  - MTA9ASF2G72PZ-2G9E1 (16GB)
* PCIe GEN4 x16 operation (16GT/s) in CCIX slot.
* PCIe GEN3 x16 link to PEX8749 Broadcom switch connecting
  3 on-board devices (USB, SATA & GbE) and 3 PCIe slots.
* 1920x1080@60Hz display support in device tree mode.

Platform Support
----------------

* This Software release is tested on r0p0 version of Morello SoC.
* This Software release is tested on Fast Model Platform (FVP)
  version - Fast Models [0.11.27 (Oct 11 2021)].

Known issues or Limitations
---------------------------

* The Morello toolchain, which is used to build Android, is based on a much
  more recent version of LLVM than the default prebuilt toolchain in Android 11.
  As a result, a large number of warnings are emitted during the build.
* While booting Android, there are warnings or error messages which are
  expected and normal given the reduced profile of the Android - ``avc`` audit
  reports, ``libprocessgroup`` missing profile messages, ``apex`` warnings,
  ``setpgid`` warnings etc.
* On Android, C++ exceptions in the Pure-cap ABI aren't supported.
* Bionic unit test ``dlfcn.segment_gap`` fails when built for the Hybrid-cap
  ABI, which is caused by executable and non-executable sections being
  allocated onto the same virtual page according to the ELF header of
  the libsegment_gap_outer.so DSO.
* On some occasions under heavy AP workloads, the SCMI mailbox kernel driver
  could timeout.

Morello Development Board specific Known Issues and Limitations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- It is recommended to run DMC Bing exclusively in Server mode. However, it
  may be possible to use Client mode for certain kinds of performance testing.
  For this type of use, we recommend that you work closely with Arm.
- The EDID data retrieval from the HDMI monitor does not work and hence the
  user needs to supply the Linux kernel with a custom EDID data using the
  kernel command line parameter drm.edid_firmware. For details on how the
  parameter works, please refer
  https://git.morello-project.org/morello/kernel/morello-ack/-/blob/morello/release-1.3/Documentation/admin-guide/kernel-parameters.txt#L1018
- PCIe root port is limited to GEN3 speed due to the on-board PCIe switch
  itself only supporting up to GEN3 speed.
- CCIX traffic is not supported over CCIX RC.
- On some occasions, multiple reboot requests or a shutdown/poweroff followed
  by a reboot request originating from AP could be left unhonoured by the
  system.
- Few vertical banding on colour changes on HDMI video output observed.
- SoC temperature sensors are not currently active. Please avoid running heavy
  workloads until these are enabled.
- The boot might hang if any USB HID input device (USB mouse / USB keyboard)
  is connected. To workaround the hang, user can trigger input from the
  connected device (any key press from a USB keyboard and/or mouse clicks from
  a USB mouse).

Tagged Version - **morello-release-1.2**
========================================

New Features
------------

The following are a summary of the key software features of this release:

* Migration of build system from Yocto to BASH based scripting environment.
* Migration of Morello Capability aware Android Common Kernel
  from 5.4.50 to 5.10.41.
* Migration of Android/Morello from Q (10.0.0_r2) to R (11.0.0_r27).
  For more details, please refer to `Android readme`_.
* Initial Linux stack config: Morello ACK + Busybox A64 rootfs (preparatory
  base for enabling future capability aware Linux environments).
* Add documentation describing support for pseudo bare-metal development
  environments. For details please refer to `Standalone baremetal readme`_.
* Rebased System Control Processor (SCP) firmware, Trusted Firmware-A,
  EDK2 and EDK2-PLATFORMS from respective upstream repositories.
* Add support for interactive debugging (with Command Line Interface)
  in the SCP RAM firmware.
  For more details, please refer to `SCP firmware readme`_.
* Add support for CMake in build-scripts to build SCP firmware. Make build is
  used by default and can be toggled to CMake by using the SCP_CMAKE_BUILD flag
  present in build-scp.sh. For more details, please refer to `Cmake readme`_.

Platform Support
----------------

* This Software release is tested on Morello Fast Model Platform (FVP)
  version - Fast Models [0.11.19 (May 5 2021)].

Known issues or Limitations
---------------------------

* For faster boot, system DRAM is limited to 2GB. For full utilization
  of 8GB DRAM, kernel command line should be edited to remove ``mem=2G``.
* The Morello toolchain, which is used to build Android, is based on a much
  more recent version of LLVM than the default prebuilt toolchain in Android 11.
  As a result, a large number of warnings are emitted during the build.
* While booting Android, there are warnings or error messages which are
  expected and normal given the reduced profile of the Android - ``avc`` audit
  reports, ``libprocessgroup`` missing profile messages, ``apex`` warnings,
  ``setpgid`` warnings etc.
* Bionic unit test ``dlfcn.segment_gap`` fails when built for the Hybrid-cap
  ABI, which is caused by executable and non-executable sections being
  allocated onto the same virtual page according to the ELF header of
  the libsegment_gap_outer.so DSO.

Tagged Version - **morello-release-1.1**
========================================

New Features
------------

The following are a summary of the key software features of this release:

* Android/Morello additional supported features:

 - Dynamic linkage support.
 - Capability bounds tightening in the standard memory allocator (jemalloc).
 - Removed derivation of capabilities from DDC after
   initialisation (replaced with the "rootcap" module).
 - Additionally ported: libjpeg-turbo ("tjbench" and other workloads),
   libpdfium ("pdfium_test" workload), libpng ("pngtest" workload),
   zlib ("zlib_example" and "minigzip" workloads),
   BoringSSL ("boringssl_ssl_test" and "boringssl_crypto_test" workloads).
 - For details please refer to `Android readme`_.

* Add support for installation and boot of standard Ubuntu 20.04 Distribution.
  For details please refer to `distribution readme`_.
* Add support for VirtioP9 Device and Virtio Net FVP components.
* Rebased System Control Processor (SCP) firmware, Trusted Firmware-A,
  EDK2 and EDK2-PLATFORMS from respective upstream repositories.
* Updated Grub to release tag grub-2.06-rc1 to incorporate
  fixes for the vulnerabilities.
  For details please refer to `GRUB2 vulnerabilities`_.

Platform Support
----------------

* This Software release is tested on Morello Fast Model Platform (FVP).

Known issues or Limitations
---------------------------

* For faster boot, system DRAM is limited to 2GB. For full utilization
  of 8GB DRAM, kernel command line should be edited to remove ``mem=2G``.
* The Morello toolchain, which is used to build Android, is based on a much
  more recent version of LLVM than the default prebuilt toolchain in Android 10.
  As a result, a large number of warnings are emitted during the build.
* While booting Android, there are warnings or error messages which are
  expected and normal given the reduced profile of the Android - ``avc`` audit
  reports, ``libprocessgroup`` missing profile messages, ``apex`` warnings,
  ``setpgid`` warnings etc.
* Bionic unit test ``dlfcn.segment_gap`` fails when built for the Hybrid-cap
  ABI, which is caused by executable and non-executable sections being
  allocated onto the same virtual page according to the ELF header of
  the libsegment_gap_outer.so DSO.

Tagged Version - **morello-release-1.0**
========================================

New Features
------------

The following are a summary of the key software features of this release:

* Android Common Kernel 5.4.50 with Morello Capability Enablement.
  For details please refer to `Kernel Morello documentation`_.
* Android Q (10.0.0_r2) nano profile with some components ported to
  Morello Pure-cap ABI. For details please refer to `Android readme`_.
* LLVM Toolchain support for Morello Platform.
  For details please refer to `Toolchain readme`_.
* Yocto based BSP build supporting Poky Distribution (AArch64).
  Android build scripts.
* Morello Capability aware platform support in Trusted Firmware-A.
* Morello Capability aware platform support in EDK2.
* System Control Processor (SCP) firmware for power & clock control and
  for memory controller & interconnect configuration.

Platform Support
----------------

* This Software release is tested on Morello Fast Model Platform (FVP).

Known issues or Limitations
---------------------------

* For faster boot, system DRAM is limited to 2GB. For full utilization
  of 8GB DRAM, kernel command line should be edited to remove ``mem=2G``.
* The Morello toolchain, which is used to build Android, is based on a much
  more recent version of LLVM than the default prebuilt toolchain in Android 10.
  As a result, a large number of warnings are emitted during the build.
* While booting Android, there are warnings or error messages which are
  expected and normal given the reduced profile of the Android - ``avc`` audit
  reports, ``libprocessgroup`` missing profile messages, ``apex`` warnings,
  ``setpgid`` warnings etc.

.. _Kernel Morello documentation:
 https://git.morello-project.org/morello/kernel/morello-ack/-/blob/morello/release-1.0/Documentation/arm64/morello.rst

.. _this section of the Morello kernel documentation:
 https://git.morello-project.org/morello/kernel/linux/-/blob/morello-release-1.5.0/Documentation/arm64/morello.rst#abis

.. _Android readme:
 android-readme.rst

.. _Toolchain readme:
 toolchain-readme.rst

.. _distribution readme:
 distro.rst

.. _GRUB2 vulnerabilities:
 https://lists.gnu.org/archive/html/grub-devel/2021-03/msg00007.html

.. _Standalone baremetal readme:
 standalone-baremetal-readme.rst

.. _SCP firmware readme:
 https://git.morello-project.org/morello/scp-firmware/-/blob/morello/release-1.2/readme.md

.. _Cmake readme:
 https://git.morello-project.org/morello/scp-firmware/-/blob/morello/release-1.2/doc/cmake_readme.md

.. _Troubleshooting Guide:
 https://git.morello-project.org/morello/docs/-/blob/morello/release-1.4/troubleshooting-guide.rst

.. _ROM Binaries:
 https://git.morello-project.org/morello/rom-binaries/-/tree/morello/release-1.5
