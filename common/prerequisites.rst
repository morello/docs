*************
Prerequisites
*************

Host prerequisites for a validated build environment
----------------------------------------------------

- Ubuntu Linux 20.04 LTS running on an x86_64 machine.
- Minimum 16GB of RAM and 250GB free storage space.
- Commands provided in this guide are executed from a ``bash`` shell
  environment.


Packages
--------

To ensure that all the required packages are installed, run:

::

    sudo apt-get update
    sudo apt-get install autoconf autopoint bc binfmt-support bison \
        build-essential ca-certificates cpio curl debootstrap \
        device-tree-compiler docker.io dosfstools doxygen fdisk flex gdisk \
        gettext-base git libncurses5 libssl-dev libtinfo5 \
        linux-libc-dev-arm64-cross lsb-release m4 mtools pkg-config \
        python-is-python3 python3-distutils qemu-user-static rsync snapd \
        unzip uuid-dev wget

To ensure that all the required packages for FVP are installed, run:

::

    sudo apt-get install telnet xterm

Refer to `Android pre-requisites`_ for more information on packages required to
build Android.

CMake
-----

While ``cmake`` is available to install with ``apt-get``, this
does not install the required version i.e. 3.18.4 or later.
Following commands can be used for checking the cmake version and removing
it if the version is less than 3.18.4.

::

    sudo cmake --version
    sudo apt-get remove cmake

An alternative way to install the cmake tool is by running the
following command.

::

    sudo snap install cmake --classic

Repo
----

Follow the steps mentioned below to install the ``repo`` tool:

::

    mkdir ~/bin/
    PATH="${HOME}/bin:${PATH}"
    export REPO=$(mktemp /tmp/repo.XXXXXXXXX)
    curl -o ${REPO} https://storage.googleapis.com/git-repo-downloads/repo
    gpg --keyserver keys.openpgp.org --recv-key 8BB9AD793E8E6153AF0F9A4416530D5E920F5C65
    curl -s https://storage.googleapis.com/git-repo-downloads/repo.asc | gpg --verify - ${REPO} && install -m 755 ${REPO} ~/bin/repo


**Note:** In case of any failure or for the latest set of instructions to
install the ``repo`` tool, refer to the `AOSP guide`_.

The ``repo`` tool uses ``git`` to download the source code which
should be configured before using the ``repo`` tool.

::

    git config --global user.name "Your Name"
    git config --global user.email "you@example.com"


.. _AOSP guide:
 https://source.android.com/setup/develop#installing-repo

.. _Android pre-requisites:
 https://source.android.com/setup/build/initializing#setting-up-a-linux-build-environment
