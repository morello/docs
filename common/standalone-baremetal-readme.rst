#######################################
Standalone Baremetal Application Readme
#######################################

.. section-numbering::
    :suffix: .

.. contents::

A baremetal application can be run on the Morello platform in either of the
following two ways during the firmware boot stage.

1. Post SCP firmware boot: Once the Application core comes out of reset,
   it starts executing the standalone baremetal application at EL3.
   Basic system initialization will be done by standalone baremetal
   application itself. This approach does not require any modification
   to the standard stack build and is described in the
   `Arm Development Studio Morello Edition documentation`_.

2. As a TF-A payload: CPU initialization will be performed by BL31 at
   EL3 and then standalone baremetal application runs at EL2.

This document explains how to run a simple baremetal application as a
BL33 TF-A payload. In the Morello platform, a standalone baremetal application
can be run at EL2 as a BL33 payload (non-secure payload running after the TF-A
binaries have been executed).

This page describes how to

* create a standalone baremetal application code.
* build and link into a final standalone executable.
* package the standalone application.
* execute the packaged application as BL33 payload at EL2 on the Morello
  platform.


Create standalone application code
==================================

Create the standalone C file (``helloworld.c`` file). In this C file,
the main function is writing characters ('h','e','l','l','o') to UART data
register one by one (BL31 would have already initialized the BOOT UART
device, so now hello world application can directly send given character to
UART TX register). Create a directory called ``helloworld`` first and then
create ``helloworld.c`` inside this directory. The content of the C file
is below.

.. code-block:: c

  #include <stdio.h>

  #define PLAT_ARM_BOOT_UART_BASE 0x2A400000

  volatile char * ptr = (volatile char*) PLAT_ARM_BOOT_UART_BASE;

  int main()
  {
    *ptr='h';
    *ptr='e';
    *ptr='l';
    *ptr='l';
    *ptr='o';
    while (1);
    return 0;
  }

Create linker script
====================

Create the linker script. This linker script sets the MEMORY field to
``0xE000_0000`` (``PLAT_ARM_NS_IMAGE_BASE``) and creates a SECTIONS field
starting at the same address. ``PLAT_ARM_NS_IMAGE_BASE`` is the destination
address intended for the non-secure payload which is copied from
AP's QSPI NOR Flash memory (emulated for FVP) by BL2. Create a linker script
in the same path where the ``helloworld.c`` is residing.
The content of the linker script (``link_scripts.ld.S``) is shown below.

.. code-block:: none

  OUTPUT_FORMAT("elf64-littleaarch64")
  OUTPUT_ARCH("aarch64")
  ENTRY(main)

  MEMORY {
    RAM (rwx): ORIGIN = 0xE0000000, LENGTH = 0xE0000000 + 0x200000
  }

  SECTIONS
  {
    . = 0xE0000000;

    ro . : {
      */helloworld.o(.text)
      *(.text*)
      *(.rodata*)
    } >RAM

    .data : {
      *(.data*)
    } >RAM
  }

Install LLVM toolchain
======================

Install the toolchain on the x86-x64 Linux Host machine to build the
baremetal application. Morello platform has a dedicated LLVM toolchain.
Refer to `Toolchain readme`_ for fetching the LLVM toolchain for Morello.
Fetch the ``morello/baremetal-release-X.Y`` toolchain branch for building
the standalone baremetal application.

Build standalone application
============================

Compile the standalone baremetal application with the LLVM toolchain
by executing the following steps:

.. code-block:: sh

  $ cd helloworld
  <absolute path of toolchain>/bin/clang -target aarch64-none-elf -c helloworld.c -o helloworld.o -O3
  <absolute path of toolchain>/bin/ld.lld -o helloworld -T link_scripts.ld.S helloworld.o -s
  <absolute path of toolchain>/bin/llvm-objcopy -O binary helloworld

This will generate a binary called ``helloworld``. This is the binary intended
to run as a non-secure payload on the Morello platform.

Generate supporting binaries
============================

This can be divided into 2 steps:

1. Generate the supporting binaries since they are required to run the
   baremetal application. These are produced by the standard stack build
   scripts. Refer to the `Firmware user guide`_ for generating supporting
   binaries.

2. From ``<morello_workspace>`` directory execute the following
   commands to package the ``helloworld`` binary:

.. code-block:: sh

  $ make -C "bsp/arm-tf" \
  PLAT=morello TARGET_PLATFORM=<fvp/soc> ENABLE_MORELLO_CAP=<0/1> \
  CC="<morello_workspace>/tools/clang/bin/clang" clean

  $ MBEDTLS_DIR="<morello_workspace>/bsp/deps/mbedtls" \
  CROSS_COMPILE="<morello_workspace>/tools/clang/bin/llvm-" \
  make -C "bsp/arm-tf" \
  CC="<morello_workspace>/tools/clang/bin/clang" \
  LD="<morello_workspace>/tools/clang/bin/ld.lld" \
  PLAT=morello ARCH=aarch64 TARGET_PLATFORM=<fvp/soc> ENABLE_MORELLO_CAP=<0/1> \
  E=0 TRUSTED_BOARD_BOOT=1 GENERATE_COT=1 ARM_ROTPK_LOCATION="devel_rsa" \
  ROT_KEY="plat/arm/board/common/rotpk/arm_rotprivk_rsa.pem" \
  BL33="<morello_workspace>/helloworld/helloworld" \
  OPENSSL_DIR="<morello_workspace>/output/<fvp/soc>/intermediates/host_openssl/install" \
  all fip

The commands above will generate ``fip.bin`` and ``bl1.bin`` under the
``<morello_workspace>/bsp/arm-tf/build/morello/release/`` directory.

**Note:**

- ``TARGET_PLATFORM`` takes 'fvp' for FVP and 'soc' for Development Board.
- ``ENABLE_MORELLO_CAP`` can take either 0 or 1.

More information can be found at `Trusted Firmware Build options`_

Execute the standalone application
==================================

Running on FVP
--------------

Get the latest FVP model for the Morello platform.
Refer to the section ``Running the software on FVP`` in the `user guide`_.

Launch FVP with the necessary binaries by

.. code-block:: sh

  $ <absolute path of model>/FVP_Morello/models/Linux64_GCC-6.4/FVP_Morello \
  --data Morello_Top.css.scp.armcortexm7ct=<morello_workspace>/bsp/rom-binaries/scp_romfw.bin@0x0 \
  --data Morello_Top.css.mcp.armcortexm7ct=<morello_workspace>/bsp/rom-binaries/mcp_romfw.bin@0x0 \
  -C Morello_Top.soc.scp_qspi_loader.fname=<morello_workspace>/output/fvp/firmware/scp_fw.bin \
  -C Morello_Top.soc.mcp_qspi_loader.fname=<morello_workspace>/output/fvp/firmware/mcp_fw.bin \
  -C css.scp.armcortexm7ct.INITVTOR=0x0 \
  -C css.mcp.armcortexm7ct.INITVTOR=0x0 \
  -C css.trustedBootROMloader.fname=<morello_workspace>/bsp/rom-binaries/bl1.bin \
  -C board.ap_qspi_loader.fname=<morello_workspace>/bsp/arm-tf/build/morello/release/fip.bin \
  -C css.pl011_uart_ap.out_file=uart0.log \
  -C css.scp.pl011_uart_scp.out_file=scp.log \
  -C css.mcp.pl011_uart0_mcp.out_file=mcp.log \
  -C css.pl011_uart_ap.unbuffered_output=1

Running on Development Board
----------------------------

Refer to the ``Running the Software on Development Board`` section in
the `user guide`_ to run the generated binaries.

**Note:** Use ``fip.bin`` from
``<morello_workspace>/bsp/arm-tf/build/morello/release/fip.bin``

Verifying the execution
=======================

Check for the following console message in

- ``uart0.log`` for FVP
- ``AP console`` for Development Board

.. code-block:: none

  NOTICE:  Booting Trusted Firmware
  NOTICE:  BL1: v2.4(debug):37a5f702b
  NOTICE:  BL1: Built : 14:51:05, Jan 27 2021
  INFO:    BL1: RAM 0x4074000 - 0x407f000
  INFO:    Using crypto library 'mbed TLS'
  INFO:    Loading image id=6 at address 0x4001010
  INFO:    Image id=6 loaded: 0x4001010 - 0x40014ce
  INFO:    Loading image id=31 at address 0x4001010
  INFO:    Image id=31 loaded: 0x4001010 - 0x400119c
  INFO:    FCONF: Config file with image ID:31 loaded at address = 0x4001010
  INFO:    Loading image id=24 at address 0x4001300
  INFO:    Image id=24 loaded: 0x4001300 - 0x40013e8
  INFO:    FCONF: Config file with image ID:24 loaded at address = 0x4001300
  INFO:    BL1: Loading BL2
  INFO:    Loading image id=1 at address 0x4057000
  INFO:    Image id=1 loaded: 0x4057000 - 0x4068961
  NOTICE:  BL1: Booting BL2
  INFO:    Entry point address = 0x4057000
  INFO:    SPSR = 0x3c5
  NOTICE:  BL2: v2.8(release):v2.8-462-gc8683ee92
  NOTICE:  BL2: Built : 14:34:33, Mar 23 2023
  NOTICE:  BL1: Booting BL31
  INFO:    Entry point address = 0xff000000
  INFO:    SPSR = 0x3cd
  NOTICE:  BL31: v2.8(release):v2.8-462-gc8683ee92
  NOTICE:  BL31: Built : 14:34:40, Mar 23 2023
  hello

.. _Toolchain readme:
 ../toolchain-readme.rst

.. _Firmware user guide:
  ../firmware/user-guide.rst

.. _Arm Development Studio Morello Edition documentation:
 https://developer.arm.com/documentation/102233/latest

.. _Trusted Firmware Build options:
 https://trustedfirmware-a.readthedocs.io/en/latest/getting_started/build-options.html#common-build-options
