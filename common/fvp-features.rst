####################
Morello FVP Features
####################

VirtioP9 Device
===============

Morello FVP provides support for virtio p9 device. This enables accessing
a directory on the host's filesystem within Linux, or another operating system
that implements the protocol.

In run_model.sh -v, --virtiop9-enable flag is used to enable directory sharing
between host OS and FVP. This flag takes the path to the host directory to be
shared. The directory to be shared must already be present on the host OS.

::

        # Host OS
        mkdir <morello_workspace>/shared_folder
        # If the folder to be shared already exists then no need to create it and
        # just pass the path to it with -v flag.
        ./run-scripts/run_model.sh -m <model binary path> -f <busybox/android-nano/android-swr> -v <path/to/shared_folder>
        #
        # Inside FVP
        mkdir shared_folder
        mount -t 9p -o trans=virtio,version=9p2000.L FM shared_folder

Virtio Net Component
====================

Morello FVP provides support for Virtio Net component along with smc91x
component. Virtio Net provides much better network performance than the
SMSC_91C111 component, because it features host-assisted network acceleration.
This means that it can offload packet processing operations from the simulated
OS on the target, to the host side.

In run_model.sh -e, --ethernet selects the ethernet
driver to use (default: virtio_net).

::

      ./run-scripts/run_model.sh -m <model binary path> -f <busybox/android-nano/android-swr> -e <smc91x/virtio_net>

Setting up TAP interface (optional)
===================================

The Morello FVP supports ethernet interface to allow networking
support to be usable for the software executed by the FVP. If the
FVP is to be represented as a discrete network entity, the host TAP interface
has to be set up before the FVP is launched. To set up the TAP interface,
execute the following commands on the host machine.

- Install libvirt

  ::

    sudo apt-get install libvirt-daemon-system

- Ensure that the libvirtd service is active.

  ::

    sudo systemctl start libvirtd

- Use the ifconfig command and ensure that a virtual bridge interface named
  'virbrX' (where X is a number 0,1,2,....) is created.

- Create a tap interface named 'tap0'

  ::

    sudo ip tuntap add dev tap0 mode tap user $(whoami)
    sudo ifconfig tap0 0.0.0.0 promisc up
    sudo brctl addif virbr0 tap0

- Launch the model with -t option for TAP interface.

  ::

    ./run-scripts/run_model.sh -m <model binary path> -f <busybox/android-nano/android-swr> -e <smc91x/virtio_net> -t tap0

**Note:** If TAP interface is not set, then user mode networking is enabled by
default. Refer to `User mode networking`_ for more details.


.. _User mode networking:
 https://developer.arm.com/documentation/100964/1116/Introduction-to-the-Fast-Models-Reference-Manual/User-mode-networking