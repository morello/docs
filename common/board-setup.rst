############################
Setting up the Morello Board
############################

**WARNING**
::

     - The Morello System Development Platform is intended for use within a
       laboratory or engineering development environment. Do not use the
       Morello System Development Platform near equipment that is sensitive to
       electromagnetic emissions, for example, medical equipment.
     - Never subject the board to high electrostatic potentials.
       Observe Electrostatic Discharge (ESD) precautions when handling any board.

          - Always wear a grounding strap when handling the board.
          - Avoid touching the component pins or any other metallic elements.


Follow these steps to setup the board:

* Connect a USB-B cable between the host machine and Morello's
  USB DBG port on the back panel.
* Connect a LAN cable to the PCIe GbE LAN port that is above
  the 2 USB3 host ports.
* Connect an HDMI cable if display is required.
* Connect the supplied C13 power cable to the C14 socket on the rear of
  the platform.
* Switch-on the ATX PSU using the switch next to the C14 socket at the rear of
  the platform.
* After power on, the board will enumerate 8 COM ports (user might need to
  wait for a few minutes, depending on the host system).
* Note down the newly enumerated COM port numbers. Assuming that the COM
  ports start at ttyUSB<n>, following is the COM port assignment:

.. list-table::
   :widths: 25 35 40
   :header-rows: 1

   * - COM Port
     - FTDI device serial number/port
     - Description
   * - ttyUSB<n>
     - 00FT<platform-serial-num>B(0)
     - Motherboard Configuration Controller(MCC)
   * - ttyUSB<n+1>
     - 00FT<platform-serial-num>B(1)
     - Platform Controller Chip(PCC)
   * - ttyUSB<n+2>
     - 00FT<platform-serial-num>B(2)
     - Application Processor(AP)
   * - ttyUSB<n+3>
     - 00FT<platform-serial-num>B(3)
     - System Control Processor(SCP)
   * - ttyUSB<n+4>
     - 00FT<platform-serial-num>A(0)
     - Manageability Control Processor(MCP)
   * - ttyUSB<n+5>
     - 00FT<platform-serial-num>A(1)
     - IOFPGA UART0
   * - ttyUSB<n+6>
     - 00FT<platform-serial-num>A(2)
     - IOFPGA UART1
   * - ttyUSB<n+7>
     - 00FT<platform-serial-num>A(3)
     - AP Secure UART

* Open the MCC, AP and SCP COM ports using a serial port application such as
  *minicom* with the following settings:

      ::

               115200 baud
               8-bit word length
               No parity
               1 stop bit
               No flow control

**Note**: Some serial port applications, including *minicom*, refer to this as
"115200 8N1" or similar.

* An example usage to open the MCC console using *minicom* is as follows:
  (Ensure to have *minicom* package pre-installed to the host using
  ``"sudo apt-get install minicom"``)

  ::

               sudo minicom -s /dev/ttyUSB<n>

  Select ``Serial port setup`` from the user interface and configure the port
  settings mentioned above. Select ``Exit`` to apply the settings and to proceed
  with the port connection.

  Refer to the COM port nomenclature in the table above to choose the desired
  port to establish the connection with. For more information on *minicom*
  usage, refer to `Minicom Manual`_.

* In the MCC console, run USB_ON command. This will launch the microSD card
  in the Morello board as a mass storage device in the host PC.

         ::

               Cmd> USB_ON

* Enter the following command on the MCC console window to ensure date and time
  is correctly set, based on the UTC. This is a one time setting and is required
  when booting the board for the first time. If the date and time values are
  incorrect, set them to the correct UTC-based values.

      ::

             Cmd> debug
             Debug> time
             Debug> date
             Debug> exit


Using on-board SATA drive
=========================

The Morello board has a SATA drive connected to the onboard SATA connector,
secured and packaged in an ATX case.

The SATA drive can be used as a boot device by flashing a bootable disk image
onto it and then booting off it. The prerequisite is that a working Linux kernel
image (preferably busybox) should first be booted into - either by means of a
bootable USB drive or via Network boot - as explained in the earlier sections.

Once booted to Linux, follow the steps below to flash an bootable disk image to
the SATA drive:

    ::

        $ wget -O </dev/sdX> <NETWORK/PATH/TO/IMAGE>      # Fetch and flash
                                                          # the disk image to
                                                          # SATA drive
        $ sync

**Note:** Replace ``</dev/sdX>`` with the handle corresponding to your SATA
drive.

.. _Minicom Manual:
 http://manpages.ubuntu.com/manpages/trusty/man1/minicom.1.html
