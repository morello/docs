##################
Android User Guide
##################

.. section-numbering::
    :suffix: .

.. contents::

Prerequisites
=============

Start by installing the necessary host PC packages as mentioned in the `Prerequisites`_
section

Syncing the Source Code
=======================

Create a new folder that will be your workspace.
All the instructions below assume that this directory is the workspace
directory.

::

    mkdir <morello_workspace>
    cd <morello_workspace>


Run the following commands to fetch the software stack sources.

**Note:** To have a shallow fetch of the stack use ``--depth=1`` with the
following *repo init* command. This fastens the repo sync process but results
in providing the limited commit history for the projects fetched.

::

    repo init \
        -u https://git.morello-project.org/morello/manifest.git \
        -b <BRANCH> \
        -g android
    repo sync

Supported options for ``<BRANCH>`` are:

* morello/integration-<major>.<minor>
  (This option fetches the release branch of a given major and minor version)
* morello/release-<major>.<minor>
  (This option fetches the release branch of a given major and minor version
  for releases delivered up to and including Release-1.5)
* refs/tags/<TAG>
  (This option fetches a particular release tag pointed by the <TAG> field
  for the releases delivered up to and including Release-1.5)
* morello/mainline
  (This option fetches the mainline branch used for code development and could
  contain patches that are not available in the above branches/tags.)

**Note:** ``repo sync`` may take significant amount of time to sync the
software stack based on the host machine and network connectivity.

Building the Source Code
========================

All software components are built using a set of bash based build scripts which
are checked out under ``<morello_workspace>/build-scripts`` directory. These
scripts are common for both FVP and Development Board and the target is
differentiated using build arguments which is explained in the coming section.

Check dependencies
------------------

Before starting the software build, ensure that all the required packages are
installed on the host machine by running the following command from
``<morello_workspace>`` directory.

::

    ./build-scripts/check_dep.sh


**Note:** Add the argument ``fvp`` to the above mentioned command for checking
the FVP specific dependencies as well.

A message of "no missing dependencies detected" indicates that all the
required packages are installed on the host machine.

Building the Software Stack
---------------------------

To build all the software components for the android platform, a top level
``build-all.sh`` script is provided. Execute the following command to trigger
the software build.

::

    ./build-scripts/build-all.sh -p <PLATFORM_TYPE> -f <android-nano/android-swr> <CMD>

Supported options for ``<PLATFORM_TYPE>`` are:

* fvp
* soc

If -p flag is not passed, the build scripts default to ``fvp``
for ``PLATFORM_TYPE``.

Supported options for ``<CMD>`` are:

* clean
* build
* all

If ``CMD`` option is not passed, ``build`` is performed by default.

**Note:** If -p flag is changed, ``clean`` should be performed before a
``build``.


Android Nano (android-nano)
"""""""""""""""""""""""""""

This profile is a bare-bone console-only environment suitable for testing
terminal-only applications.

::

    ./build-scripts/build-all.sh -p <fvp/soc> -f android-nano

Android SWR (android-swr)
"""""""""""""""""""""""""

This profile provides a full Android configuration booting to Home screen.
The graphics are not accelerated and `SwiftShader`_ is used for software
rendering instead.

::

    ./build-scripts/build-all.sh -p <fvp/soc> -f android-swr

For information on Android boot and further details, please refer to `Android
readme`_.


Running the software
====================

Running the Software on the FVP
-------------------------------

The Morello Fixed Virtual Platform (Morello FVP) must be available
to test the software.

Please refer to `Arm Ecosystem FVPs`_ to download the Morello FVP.

**Run scripts:**

``<morello_workspace>/run-scripts/`` provides scripts
for running the software stack on the Morello FVP platform.


The run-scripts structure is as follows:

::

    run-scripts
      |--run_model.sh
      |-- ...

Ensure that all the dependencies are met by executing the
FVP: ``./path/to/FVP_Morello/models/Linux64_GCC-6.4/FVP_Morello``.
You should see the FVP launch, presenting a graphical interface
showing information about the current state of the FVP.

The ``run_model.sh`` script in ``<morello_workspace>/run-scripts``
will launch the FVP, providing the previously built images as arguments.
Execute the ``run_model.sh`` script:

::

        # For running Android Nano:
        ./run-scripts/run_model.sh -m <model binary path> -f android-nano

        # For running Android SWR:
        ./run-scripts/run_model.sh -m <model binary path> -f android-swr

When the script is executed, five UART terminal instances will be launched, one
for the SCP, one for MCP and three for the AP (Application Processor). Once the
FVP is running, the SCP will be the first to boot, bringing the AP out of reset.
The AP will start booting Trusted Firmware-A, then UEFI, then Android.

**Note**: Please note that booting Android SWR can take a significant amount of
time. Depending on the hardware on which the FVP is running, it can take
between 45 minutes to 2 hours until the Home screen is displayed.

To run terminal-only mode on hosts without graphics/display:

::

        # For running Android Nano:
        env -u DISPLAY ./run-scripts/run_model.sh -m <model binary path> -f android-nano

        # For running Android SWR:
        env -u DISPLAY ./run-scripts/run_model.sh -m <model binary path> -f android-swr

This launches FVP in the background and automatically connects to the
interactive application console via telnet.

To stop the model, exit telnet:

::

        Ctrl + ]
        telnet> close


For other FVP related usecases please see `Morello FVP Features`_.


Running the software on Development Board
-----------------------------------------

Please follow the `Setting up the Morello Board`_ guide before continuing.

Prepare a bootable disk
"""""""""""""""""""""""

* A bootable disk can be prepared by flashing the image generated from the
  source build onto a USB drive.
* Connect bootable disk (USB drive) into a host machine that
  is running Linux.
* Use the following commands to prepare the GRUB image on the
  USB drive

::

    $ lsblk
    $ sudo dd if=<morello_workspace>/output/soc/<android-nano.img/android-swr.img> of=</dev/sdX> conv=fsync bs=1M # write the disk image to
                                                                                                                  # /dev/sdX
    $ sudo sgdisk -e </dev/sdX>                                                                                   # adjust the primary and
                                                                                                                  # alternate header on /dev/sdX
    $ sync

**Note:** Replace ``</dev/sdX>`` with the handle corresponding to your USB drive,
as identified by the ``lsblk`` command.

**Note:** All supported images are partitioned using GPT.
For details on issues related to the location of the alternate header, refer
to the `Troubleshooting guide`_.

Booting the board with Android Image
""""""""""""""""""""""""""""""""""""

Insert the bootable disk created earlier to the Morello board. Reboot the
board by issuing the following command in the MCC console:

::

    Cmd> REBOOT

Switch to the AP UART console. Once the application core has started
booting the UEFI firmware, enter the UEFI menu by pressing the Esc
key within the 10s UEFI boot timeout. Enter the UEFI Boot Manager
menu and select the disk.

::

    | Booting `Android Morello SoC (Device Tree)'          |

The system will boot into an Android environment.

Running the Android pure-cap workloads
--------------------------------------

To run Android Test Suites and workloads ported to the Pure-cap ABI,
please refer to `Workloads`_.

Running 3rd party applications
------------------------------

See the `Porting Guidelines for 3rd party applications`_ section


.. _Arm Ecosystem FVPs:
 https://developer.arm.com/tools-and-software/open-source-software/arm-platforms-software/arm-ecosystem-fvps
.. _Android readme: android-readme.rst
.. _Prerequisites: ../common/prerequisites.rst
.. _Morello FVP Features: ../common/fvp-features.rst
.. _Setting up the Morello Board: ../common/board-setup.rst
.. _SwiftShader: https://opensource.google/projects/swiftshader
.. _Troubleshooting guide: ../common/troubleshooting-guide.rst
.. _Workloads: workloads.rst
.. _Porting Guidelines for 3rd party applications: porting-guidelines.rst
