#########################
Android/Morello Changelog
#########################

.. section-numbering::
    :suffix: .

.. contents::

This contains an overview of the changes in Android/Morello for each
integration drop.

Morello integration drop 1.6.1
==============================

Notes
-----

Future releases of Android/Morello have been postponed. There are no plans
to release further updates for Android/Morello in the foreseeable future.

Morello integration drop 1.6
============================

Summary
-------

* bionic:
   - Further work towards PCuABI compatibility: support for VMem and PROT_MAX,
     including clearing the VMem permission from capabilities in the linker and
     allocator
* libshim:
   - Further work towards memory management aspects of PCuABI support including
     PROT_MAX and VMem
   - Fix capability bounds of AT_ENTRY
   - Enable LIBSHIM_ZERO_DDC to clear the DDC once libshim initialization is
     complete
* Misc:
   - Update to v5.18 + Morello kernel headers (various repositories)
   - Option for building Android/Morello without libshim - a step towards
     supporting PCuABI kernel

Notes
_____

* The libshim PCuABI support for memory management is only partially complete
  with further compliance in progress (subject to limitations of the
  translation layer) so unexpected behaviour may occur if testing edge cases.

See `Release Notes`_ for information on older revisions

.. _Release Notes: ../deprecated/release-notes.rst

