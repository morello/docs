*********************************************
Porting Guidelines for 3rd party applications
*********************************************

.. section-numbering::
    :suffix: .

.. contents::


Target audience
===============

This guide is for developers wanting to port their own application to run as a
pure-capability binary on the Android Nano / Morello environment using Linux as
the host operating system. This guide intends to serve as a complete guide.
However, since the instructions for various steps already are documented, there
will be links created for those instead of duplicating the information.

Get the Morello FVP
===================
Arm provides the Morello FVP at the `Arm Ecosystem FVPs`_ page. Expand the
"Morello Platform FVP" section and download the Linux binary. Once downloaded,
unpack it a location of your own preference.

Sourcing & Building the Source Code
===================================

Follow the `Android User Guide`_ to source and build the source code before
continuing.

Boot up the system
==================

This step isn't strictly necessary, but to ensure that we have a stable
environment before trying to add our own work, it's recommended to boot it up
once after having built the environment, mostly as a sanity check.

There are two ways to boot up the environment, one where several windows are
spawned and another way where no windows are spawned. In the latter, the boot log
is kept in the current shell. In this guideline we're only interested in the
Android environment, therefore we'll use the latter. To boot up the environment,
run:

.. code-block:: bash

    $ export MODELPATH=/<path-to-my-models>/Morello_FVP/models/Linux64_GCC-6.4/FVP_Morello
    $ env -u DISPLAY ./run-scripts/run_model.sh -m $MODELPATH -f android-nano

Once up and running, ensure that you reach a prompt looking like this.

.. code-block:: bash

    console:/


Hello World for Morello
=======================

To build your own application as a pure-capability binary, there are a couple
of steps that needs to take place. The source code needs to be placed in the
tree, you need to write a Blueprint file (an Android "Makefile") and if you want
to have it built and included by default, then you also need to include it in
the device configuration. If the last point isn't of interest, then you can
build the application using the command line:

.. code-block:: bash

    $ m <module_name>

Further down you'll see examples of how that can be achieved.


Source code location
--------------------

There are many possibilities where to place the code. For vendor specific code,
you will typically find it under ``vendor/<vendor-name>/...``. For external code
bases these are typically stored under ``external/...``. In this guide we will
refer to our example project at ``vendor/arm/morello-examples/hello-morello``.
That is a simple hello-world type of application. As you can see that only
contains a few files:

.. code-block:: none

    .
    ├── Android.bp
    ├── include
    │   └── main.h
    └── main.c


Blueprint file
--------------

Some time ago Google started to use the `Soong build system`_, which is a
replacement for the old Android make-based build system. This means that
developers should write the build configuration in Blueprint files instead of
the old ``Android.mk`` files. Below is a snapshot of the Blueprint file for
building ``hello-morello`` as a pure-capability binary.

.. code-block:: none

	cc_binary {
		name: "hello-morello",

		srcs: ["main.c"],

		cflags: [
			"-Werror",
			"-Wno-macro-redefined"
			"-Werror=cheri",
			"-Werror=cheri-inefficient",
			"-Werror=cheri-pedantic",
			"-Werror=cheri-pointer-conversion",
			"-Werror=cheri-unimplemented-features",
		],

		local_include_dirs: ["include"],
		static_executable: true,
		compile_multilib: "c64",
	}

The first line, ``cc_binary`` tells what kind of module we're building, here a
C/C++ based binary meant to run on the device. Next is the ``name``. Every
module must have a name and it should be unique. This name is also the name that
you will use when building just the application from command line. I.e.,
building just ``hello-morello`` can be achieved by simply typing (also see
"Build the application" below for a complete example):

.. code-block:: bash

    $ m hello-morello

The ``src`` variable is where you list all your source code files. ``cflags`` is
where you put all your C compiler flags. The ``local_include_dirs`` is the path
to your include files relative to the Blueprint file itself. The
``static_executable`` tells the compiler (linker) to create a static binary.
``compile_multilib`` is normally used to tell whether the binary should be
compiled for 32-bit, 64-bit or both. However, for the Morello project and to
create pure-capability binaries this **has** to be set to ``c64``. Because of
that, an easy way to figure out which applications are compiled as
pure-capability, simply grep for ``c64`` in all bp-files. For a more detailed
list of valid module types and their properties open the file
``out/soong/docs/cc.html#cc_binary`` in your build tree.


Add the application to the product
----------------------------------

To have your application built and included in the final image, you need to add
your application(s) to the ``PRODUCT_PACKAGES``, which contains a list of
modules (and APK's) to install. For the Morello Nano, this can be achieved by
adding additional ``PRODUCT_PACKAGES`` lines to the file
``device/arm/morello/morello_nano.mk`` or ``device/arm/morello/morello_swr.mk``.
Below, the ``hello-morello`` application has been added as an example.

.. code-block:: make

	...
	# User contributed
	PRODUCT_PACKAGES += \
		hello-morello


Build the application
---------------------

As already seen, with the source code at the correct location and a valid
Blueprint file, one can build with standard Android build aliases. To see the
available commands run `hmm` at the command line. In theory it should be ready
to make the build by now, but since the `Morello build script`_ does a couple of
mandatory things, like setting the path to the `clang-local` compiler etc, you
have to run the build at least once using the Morello build-script before you
can start using the standard Android build aliases. Note that when using the
build-script you don't have to do anything of the below. But if you're using
that, then you cannot build individual components. If you just want to build
your own application, then after doing a first full successful build you can
build your own application simply by doing the following:

.. code-block:: bash

    $ cd <path-to-my-android-tree>
    $ source build/envsetup.sh
    $ lunch morello_nano-eng   # or morello_swr-eng
    $ m <name-of-my-module>

Notice that here the first three lines is something you just have to do once per
newly opened shell. I.e., once initialized it's sufficient to just re-run the
last line when you need to recompile your application.

Run the application
-------------------

If you have configured your target so the application has been built into the
final image, then you can just run the binary after booting up the environment.
On the other hand if you're doing experimental work and haven't included your
application in the configuration, then the easiest way to try it out is to push
the files via ``adb``. To transfer and run the ``hello-morello`` application,
one can do this:

.. code-block:: bash

    $ adb devices
    $ adb push out/target/product/morello/system/bin/hello-morello /data/local/tmp
    $ adb shell
    morello:/ # cd /data/local/tmp
    morello:/data/local/tmp # ./hello-morello

The reason for pushing to ``/data/local/tmp`` is simply because it's a directory
where we have write permissions by default (for users). If you need to push to
other directories, you might need to remount partitions (``adb-remount``) with
read/write permission and/or disable SELinux.

.. _Android User Guide: user-guide.rst
.. _Arm Ecosystem FVPs: https://developer.arm.com/tools-and-software/open-source-software/arm-platforms-software/arm-ecosystem-fvps
.. _Morello build script: https://git.morello-project.org/morello/build-scripts/-/blob/morello/mainline/build-android.sh
.. _Soong build system: https://android.googlesource.com/platform/build/soong/+/android11-qpr1-release/README.md
.. _Workloads: workloads.rst
.. _Android readme: readme.rst
