
**********
Morello Linux User Guide
**********

.. section-numbering::
    :suffix: .

.. contents::

Overview
========

This document explains how to get started in building and running purecap
Morello applications on the latest Morello Linux kernel.

It contains links to the latest pre-built firmware, pre-built Debian disk
images, and a software development kit with all the latest Morello specific
toolchains and utilities required to build applications for Morello.

As an overview, there are 3 steps to get started:

1. Firmware images
    - https://git.morello-project.org/morello/board-firmware
    - https://git.morello-project.org/morello/fvp-firmware
2. Bootable disk images
    - https://git.morello-project.org/morello/morello-rootfs-images
3. Developing Morello applications via the SDK
    - https://git.morello-project.org/morello/morello-sdk

Platforms
=========

Instructions vary slightly depending if you have Morello Hardware or are running
on the Morello FVP (Fixed Virtual Platform).

See https://www.morello-project.org/ for more information.

Morello Hardware
----------------

See `Setting up the Morello Board <https://git.morello-project.org/morello/docs/-/blob/morello/mainline/common/board-setup.rst>`_
for details on setting up your Morello hardware.

Fixed Virtual Platform (FVP)
----------------------------

To setup a Docker container with everything you need to build and boot into a
Debian based Morello Linux environment on the FVP, in less than 10 minutes see:

https://git.morello-project.org/morello/morello-linux

**Note**: The purpose of this environment is to provide a quick to setup demo to
get people familiar with Morello and Capability concepts.

The image contains Morello toolchains, so compiling simple purecap applications
within the FVP is possible but slow. Building your own disk images with your own
purecap applications is also possible, but outside the scope of this guide.

Firmware
========

Morello Hardware
----------------

Pre-built firmware for the Morello board, based on the latest release is
available at:

https://git.morello-project.org/morello/board-firmware

The Morello board has an onboard microSD to make updating firmware easy. Simply
copy the contents of this repo onto the microSD card and reboot.

For more details and in depth instructions, see:

- `Setting up the Morello Board <https://git.morello-project.org/morello/docs/-/blob/morello/mainline/common/board-setup.rst>`_
- `Updating firmware on microSD card <https://git.morello-project.org/morello/docs/-/blob/morello/mainline/firmware/user-guide.rst#update-firmware-on-microsd-card>`_

Fixed Virtual Platform (FVP)
----------------------------

Pre-built firmware for the FVP is available at:

https://git.morello-project.org/morello/fvp-firmware

**Note**: When running the FVP via the `morello-linux <https://git.morello-project.org/morello/morello-linux>`_ Docker container, the
latest firmware is always included, so this step is not required.

Bootable disk images
====================

Pre-built bootable images for both Morello hardware and FVP are available at:

https://git.morello-project.org/morello/morello-rootfs-images

The disk images contain a bootable GPT partition with the Grub bootloader,
and EFI/DTBs required to boot into a Debian based system/rootfs, running the
latest Morello kernel. The image also contains the
`Morello SDK <https://git.morello-project.org/morello/morello-sdk>`_
with toolchains and utilities to develop Morello purecap applications directly
on the running Morello system.

Morello Hardware
----------------

Download the `latest image for Morello SoC <https://git.morello-project.org/morello/morello-rootfs-images/-/jobs/artifacts/morello/mainline/raw/morello-soc.tar.xz?job=build-morello-rootfs-images>`_.

USB
^^^

Create a bootable USB drive with the image.

On a Linux host machine:
::
    $ lsblk

    # write the disk image to dev/sdX
    $ sudo dd if=<IMAGE> of=</dev/sdX> conv=fsync bs=1M status=progress

    # adjust the primary and alternate header on /dev/sdX
    $ sudo sgdisk -e </dev/sdX>

    $ sync

**Note**: Replace ``</dev/sdX>`` with the handle corresponding to your USB drive,
as identified by the ``lsblk`` command.

**Note**: Ensure to copy the image directly on the disk, e.g. ``/dev/sdb`` and
**NOT** to a partition on the disk such as ``/dev/sdb1``

Network Boot
^^^^^^^^^^^^

The Morello UEFI is by default built with support for PXE (which uses TFTP) and
HTTP (but not HTTPS) network booting.

Network boot requires running a TFTP and DHCP server such as ``dnsmasq`` to be
able to serve the images via PXE.

For more information see
`Installing Debian using network booting <https://wiki.debian.org/PXEBootInstall>`_.

On-board SATA SSD drive
^^^^^^^^^^^^^^^^^^^^^^^

Once booted into a system, either with the Morello image above, or any aarch64
Linux distribution via USB or Network Boot, the on-board SATA SSD drive can be
flashed with the Morello image.

Due to the greatly increased I/O performance of the SSD vs USB drive, it's
recommended to run from SSD where possible.

See `Using on-board SATA drive
<https://git.morello-project.org/morello/docs/-/blob/morello/mainline/common/board-setup.rst#using-on-board-sata-drive>`_
for more info.

SSH Access
^^^^^^^^^^

The Debian image has ``sshd`` running on startup.

Connect to the AP0 console via USB Debug as per `Setting up the Morello Board
<https://git.morello-project.org/morello/docs/-/blob/morello/mainline/common/board-setup.rst>`_ and login with credentials below.
::
    Username: root
    Password: morello

    $ ip addr             # obtain the local <ip> address

This can be used to remotely login via ``ssh``, or copy files to and from a
running system with ``scp`` or ``rsync``.
::
    $ ssh root@<ip>                                         # remotely connect

    $ scp /path/to/local/file root@<ip>:/remote/dest/path   # copy to Morello board
    $ scp root@<ip>:/path/to/remote/file /local/dest/path   # copy from Morello board
    $ scp -r src dest                                       # use -r (recursive) to copy directories

**Note** When logging in via SSH, you may see
``bash: warning: setlocale: LC_ALL: cannot change locale``. SSH automatically
forwards the locale settings of your local machine. You can configure your
locale with ``dpkg-reconfigure locales`` on the board, or configure your SSH
client to stop forwarding locale environment settings.

SDK
^^^

The `Morello SDK <https://git.morello-project.org/morello/morello-sdk>`_ is
included in the disk image and found in the ``/morello`` directory. Please see
below for more information.

Morello Development Environment/SDK
===================================

The easiest way to start developing purecap applications for Morello is with the
Morello SDK/development environment found at:

https://git.morello-project.org/morello/morello-sdk

This allows both aarch64 and x86 hosts to cross-compile applications with a
Morello purecap target.

The SDK contains the latest Morello LLVM/clang, Morello musl libc and Morello
Linux kernel headers, and additional utilities. These can be obtained pre-built,
setup and ready to go inside a Docker image or downloaded and built locally.

See the project `README <https://git.morello-project.org/morello/morello-sdk>`_
for more details.
